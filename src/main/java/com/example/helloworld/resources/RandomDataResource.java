package com.example.helloworld.resources;

import com.example.helloworld.core.Saying;
import com.google.common.base.Optional;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicLong;

@Path("/randomData")
@Produces(MediaType.APPLICATION_JSON)
public class RandomDataResource {
    private final AtomicLong counter;
    private final String url_string;

    public RandomDataResource(String url_string) {
        this.counter = new AtomicLong();
        this.url_string=url_string;
    }

    @GET
    @Timed
    public Saying sayHello() {
    	URL oracle;
		String input=new String();
		//input="<test input>";
		try {
			oracle = new URL(url_string);//URL("http://104.154.93.86:9000/");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					oracle.openStream()));
			
			String inputLine;
			while ((inputLine = in.readLine()) != null)
				input=input+inputLine;
			in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	
		String response=input+" "+new StringBuilder(input).reverse();
        return new Saying(counter.incrementAndGet(),response );
    }
}